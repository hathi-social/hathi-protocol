package protocol

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"encoding/json"
	"errors"
	"io"
	"time"
	"fmt"
)

func newStreamBuffer() (s *streamBuffer) {
	s = new(streamBuffer)
	s.data = make([][]byte, 0)
	s.closed = false
	return s
}

type streamBuffer struct {
	data [][]byte
	closed bool
}

func (s *streamBuffer) Close() {
	s.closed = true
}

func (s *streamBuffer) Read(req []byte) (n int, err error) {
	requestLen := len(req)
	if requestLen == 0 {
		return 0, nil
	}
	if len(s.data) == 0 {
		// No data available
		if s.closed {
			return 0, io.EOF
		} else {
			return 0, nil
		}
	}
	fulfilled := 0
	var d []byte
	for fulfilled < requestLen {
		if len(s.data[0]) <= requestLen {
			// We can strip an entire item
			d, s.data = s.data[0], s.data[1:]
		} else {
			// Take part of the item
			d, s.data[0] = s.data[0][:requestLen], s.data[0][requestLen:]
		}
		copy(req[fulfilled:], d)
		fulfilled += len(d)
		if len(s.data) == 0 {
			break // Out of data
		}
	}
	return fulfilled, nil
}

func (s *streamBuffer) Append(data []byte) {
	s.data = append(s.data, data)
}

type EndpointKernel struct {
	// Packets are converted to their final form as soon as the endpoint
	// gets a hold of them so that errors are detected immediately.
	// The inbound *Packet is created for the chan and then the endpoint code
	// never touches it again. Ownership transfers to the caller of GetPacket.
	inbound      chan *Packet
	holding      *streamBuffer
	decoder      *json.Decoder
	conn         io.ReadWriter   // Connection
	implemented  map[string]bool // implemented function table
	lastActivity time.Time       // last reception time (for pings)
	running      bool            // Keep running the main loop
	closed       bool            // Has the connection been closed
	ping         bool            // Send pings. Will still pong if false
	pingID       int             // Last pingID sent
}

func NewEndpointKernel(connection io.ReadWriter) (e *EndpointKernel) {
	e = new(EndpointKernel)
	e.inbound = make(chan *Packet, 16) // Buffer size pulled out of hat
	e.holding = newStreamBuffer()
	e.conn = connection
	e.decoder = json.NewDecoder(e.holding)
	e.lastActivity = time.Time{}
	e.running = false
	e.closed = false
	e.ping = false
	e.pingID = 0
	e.SetImplementedFunctions([]string{})
	return e
}

func (e *EndpointKernel) SendPacket(p *Packet) (err error) {
	data, err := p.SerializePacket()
	if err != nil {
		return err
	}
	n, err := e.conn.Write(data)
	if err != nil {
		return err
	}
	if n != len(data) {
		return errors.New("Incorrect send count")
	}
	return nil
}

func (e *EndpointKernel) GetPacket() (p *Packet) {
	select {
	case p = <-e.inbound:
		return p
	default:
		return nil
	}
}

func (e *EndpointKernel) WaitForPacket() (p *Packet) {
	return <-e.inbound
}

func (e *EndpointKernel) SetImplementedFunctions(fList []string) {
	newImps := make(map[string]bool, 0)
	for _, v := range fList {
		newImps[v] = true
	}
	// Required commands
	newImps["ping"] = true
	newImps["pong"] = true
	newImps["result"] = true
	// We build the new map and then assign it so that the mainloop thread
	// doesn't run into trouble if it is running. (thread-safety)
	e.implemented = newImps
}

func (e *EndpointKernel) Run() {
	e.running = true
	go e.mainloop()
}

func (e *EndpointKernel) Stop() {
	e.running = false
}

func (e *EndpointKernel) IsClosed() (isClosed bool) {
	return e.closed
}

func (e *EndpointKernel) SetPingMode(doPings bool) {
	e.ping = true
}

func (e *EndpointKernel) mainloop() {
	data := make([]byte, 1024)
	for e.running {
		// read data, will block here
		rlen, err := e.conn.Read(data)
		if err == io.EOF {
			// Connection closed
			doLog("Connection closed", 1)
			e.closed = true
			e.Stop()
			e.holding.Close() // will close when out of data
			// Clear out any remaining packets
			for true {
				doLog("Cleaning up remaining packets", 2)
				pkt := e.decodePacket()
				doLog("Cleanup: " + fmt.Sprintln(pkt), 2)
				if pkt == nil {
					return // No more packets
				}
				e.handleIncomingPacket(pkt)
			}
			return
		} else if err != nil {
			doLogErr("Connection read error", err, 1)
			continue
		}
		e.holding.Append(data[:rlen])
		pkt := e.decodePacket()
		if pkt != nil {
			e.lastActivity = time.Now()
			e.handleIncomingPacket(pkt)
		}
	}
}

func (e *EndpointKernel) decodePacket() (pkt *Packet) {
	var datum interface{}
	if e.decoder.More() {
		err := e.decoder.Decode(&datum)
		if err != nil {
			doLogErr("Decode error:", err, 1)
		}
		pkt, err = ParseMap(datum.(map[string]interface{}))
		if err != nil {
			doLogErr("Parse error:", err, 1)
			return nil
		}
		return pkt
	} else {
		return nil
	}
}

func (e *EndpointKernel) handleIncomingPacket(p *Packet) {
	pktF := p.Function
	if pktF == "ping" {
		// send pong
		pong := new(Packet)
		pong.Version = 1
		pong.Function = "pong"
		pong.RequestID = p.RequestID
		e.SendPacket(pong)
	} else if pktF == "pong" {
		// pong handling
		// TODO: remove fron sent pings map
	} else if imp, ok := e.implemented[pktF]; ok == false || imp == false {
		// not implemented response
		noimp := new(Packet)
		noimp.Version = 1
		noimp.Function = "not-implemented"
		noimp.RequestID = p.RequestID
		e.SendPacket(noimp)
	} else {
		// Nothing special or errorful about this. Send it on.
		e.inbound <- p
	}
}
