
#
# Makefile for hathi-protocol
#

build:
	go build mock/client-mocker.go
test:
	go test ./...
govet:
	go vet ./...
gofmt goformat:
	go fmt ./...
#golint:
