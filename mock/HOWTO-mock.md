
# How To use client-mocker for testing hathi server components

client-mocker is a program for testing servers that use the hathi protocol. It
is designed to take a group of test sequences and automatically process them,
taking into account data that cannot be specified ahead of time.


# How it works

## Test sequences

Each file given to client-mocker is a self contained test sequence. It is
expected to do any setup or takedown actions that are needed to run its test.
If more than one file (or a directory of files) is given to client-mocker it
will run each file until it runs out of tests, or there is an error. There are
no guarantees about execution order between files.

NOTE: currently a bug in the multiple file / directory handling code. Other
priorities currently higher.

Each file consists of pairs of JSON objects similar to the packets that will be
sent and recieved. Each pair is seperated by a line containing nothing but "%"
(cookie-jar format).

## Request IDs

Because the order of test sequences is random it is not possible for a test to
predict what request ID a packet will have, and if it could it would force
interdependency between every sequence with every other sequence. For this
reason the request field is not included in test pairs: it does not need to be
provided, and will be ignored if it is.

## Variables

Variables allow a test sequence to use server generated data in later tests.
This allows the test to retrieve a previously posted object despite not being
able to know what that object's ID will be ahead of time.

Variables divide into 3 use cases:

1. Sending use. The variable's value in placed in a packet being sent.

2. Recieve use. The variable's value is compared to the value in a response packet.

3. Recieve set. The variable is set to the value in a recieved packet. No comparison takes place.

##worked example

{
	"version":1,
	"function":"post-object",
	"actor":"/foo",
	"object":{
		"@type":"Note",
		"content":"jabber jabber jabber"
	},
	"session":"00112233445566778899aabbccddeeff"
}
{
	"version":1,
	"function":"result",
	"object-id":"{cre=}"
}
%
{
	"version":1,
	"function":"get-object",
	"object-id":"{cre}",
	"actor":"/foo",
	"session":"00112233445566778899aabbccddeeff"
}
{
	"version":1,
	"function":"result",
	"object":{
		"@type":"Create",
		"@id":"{cre}"
		"object":{
			"@type":"Note",
			"@id":"{note=}",
			"content":"jabber jabber jabber",
		}
	}
}

This example mimics a client posting an object to the actor "/foo", and then
retreiving the posted object (the objects themselves have been greatly
simplified).

The first JSON blob is the packet that is sent to the server. `client-mocker`
will fill in the request ID field. The second blob is the expected response
packet, which sets the "cre" variable to the returned object ID (probably
something like "/foo/3094").

The "%" line is the divider between test pairs.

The second test pair is a request to get object at the ID previously stored in
"cre". Notice the "{note=}" variable set command in the returned objects. Even
though this example never uses that data the variable still needs to be set
because we cannot know what the value will be.
