*2019-03-01:*  
*We, the Hathi authors,  are continuing this project and claim copyright to the changes that we will be making henceforth.*  
*We will be continuing to publish under the Apache open source license.*  
*2019-02-28:*  
*As our sponsoring organisation has decided to spin down, it has been agreed to public-domain the Hathi project.*  
*Code created on or before this date is free for everyone to use and/or claim.*  

# hathi-protocol

Go implementation of the hathi protocol.

The protocol data itself is handled in the packet.go file. Protocol usage is
in endpoint.go.

Has support for golang's new module system. This repo may be built from a
location that is not under $GOPATH so long as golang-1.11 or greater is used.
