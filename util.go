package protocol

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"fmt"
	"log"
)

var logLevel uint

func SetLogLevel(level uint) {
	logLevel = level
}

func doLog(text string, threshold uint) {
	if threshold <= logLevel {
		log.Println(text)
	}
}

func doLogErr(label string, err error, threshold uint) {
	text := fmt.Sprintf("%s: %v", label, err)
	doLog(text, threshold)
}
